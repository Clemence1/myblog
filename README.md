# README


## création de l'application 

Pour le TP nous utiliserons sqlite3 (par default) .

Donc pas de précision de l'option `-t`

 
 ```
$ rails new MyBlog
```

**ATTENTION** ne pas oublier de créer une copie du fichier `config/database.yml` en par exemple `config/database.yml.sample`

et de mettre database.yml dnas le fichiers .gitignore ou dans un fichier git ignore de la machine, pour ne pas devoir le refaire à chaque projet. (cf git)

## Création de la base de données 

```
$ rails db:create
```

## création  du model `user` 

On choisi de créer le modèle user car il ne dépend poue le moment d'aucun autre model 

```
$ rails g scaffold user name:string:index email:string:index
``` 

le `:index` permet de créer des index sur les champs correspondant.

## migration 

```
$ rails db:migrate
```
en retour de console on obtient : 

``` 
== 20200325182804 CreateUsers: migrating ======================================
-- create_table(:users)
 -> 0.0029s
-- add_index(:users, :name)
 -> 0.0015s
-- add_index(:users, :email)
   -> 0.0021s
== 20200325182804 CreateUsers: migrated (0.0067s) =============================
```

ça a aussi créé le fichier `db/schema.rb`  qui reprensente notre base de données vu par ruby. 

On ne precise pas la colonne `:id`,  elle sera donc mise par défaut lors de la migration, si on ne souhaite pas en avoir on le précisera dans le fichier de migration, avant d'effectuer la migration dans la déclaration du `create_table`



## creation du model `blog` qui fera référence au model `user`

Un article du blog fera réference à un utilisateur.

```
rails g scaffold blog title:string content:text published_at:datetime draft:boolean:index user:references 
```

on remet ici un `:index` sur le champ `draft`  ça permettra de trier rapidement les publication en ligne ou pas.


le `user:references` va permettre de créer le champ `user_id` dans la table blog.

de placer une association `belongs_to` dans le model `Blog`

On effectue la migration pour `blog`

``` 
rails db:migrate
```

s